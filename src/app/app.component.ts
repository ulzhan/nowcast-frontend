import { Component } from '@angular/core';
import {Http} from '@angular/http';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
   spaceScreens: Array<any>;
   public docs: document[];


  constructor(private http:Http) {


      this.docs = [
      {
        author: "Samsung",
        title: "The Frame by Samsung Electronics Dazzles at La Biennale di Venezia’s 57th International Art Exhibition",
        date: "October 29, 2017 15:15",
        text: "Samsung Electronics announced today that The Frame will be exhibited in La Biennale di Venezia’ 57th International" +
        "Art Exhibition titled Viva Arte Viva, which will be held in Venice, Italy from May 13 to November 26. Samsung will provide " +
        "15 units of The Frame to be displayed throughout the event, including exhibits at the Korean Pavilion, the official press room and the VIP dining hall...",
        attachments: "https://scontent.fhen2-1.fna.fbcdn.net/v/t31.0-8/18319105_1900134173542950_7606438899334729755_o.jpg?oh=e90d90da5b374714fe29ef29a947ff03&oe=59BA84ED",
        link: "https://www.facebook.com/SmarterMeApp/photos/a.1871705553052479.1073741828.1871615299728171/1900134173542950/?type=3&theater"
      },
      {
        author: "Orgzit",
        title: "At Orgzit, we like to take a very step-by-step approach towards getting customers on-board. We let Orgzit's powerful " +
        "organizing features prove its worth in solving organizational complexity which enables our customers to better understand its worth.",
        date: "October 29, 2017 15:15",
        text: "At Orgzit, we like to take a very step-by-step approach towards getting customers on-board. We let Orgzit's powerful " +
        "organizing features prove its worth in solving organizational complexity which enables our customers to better understand its worth.",
        attachments: "https://pbs.twimg.com/media/C_W-0esVYAM7VUj.jpg",
        link: "https://twitter.com/ContextSmith/status/861804715121807360 "
      },
      {
        author: "Vtiger ",
        title: "The Project is a planned work that involves specific goals, start date and target end date",
        date: "October 29, 2017 15:15",
        text: "The Project is a planned work that involves specific goals, start date and target end date, budgets, progress, etc. When you manage projects within " +
        "Vtiger, you can also track the issues identified and fixed by associating projects with Cases module and collaborate with " +
        "team members via Comments. #VtigerHacks #Vtiger #CRM",
        attachments: null,
        link: "https://twitter.com/ContextSmith/status/861955670194761734"
      },
      {
        author: "ContextSmith company",
        title: "80% of #sales are made on or after the fifth contact",
        date: "October 29, 2017 15:15",
        text: "80% of #sales are made on or after the fifth contact. (and you can track your team's contacts with #ContextSmith!) #salesquotes",
        attachments: "https://pbs.twimg.com/media/C_WCXMGUIAAkt-o.jpg",
        link: "https://twitter.com/ContextSmith/status/861738242454016000"
      },
      {
        author: "Usermind, Inc",
        title: "Excited for the kick off of @MarTechConf",
        date: "October 29, 2017 15:15",
        text: "Hello San Francisco! Excited for the kick off of @MarTechConf tonight! #stackieawards #martech",
        attachments: "https://pbs.twimg.com/media/C_UUcPDVYAAFuj0.jpg",
        link: "https://twitter.com/usermindinc/status/862079744841482240"
      },
      {
        author: "Fullstory.com",
        title: "Consumers consider an experience \"great\"",
        date: "October 29, 2017 15:15",
        text: "Consumers consider an experience \"great\" when brands deliver on basic—and reasonable—expectations. Forrester on #CX",
        attachments: "https://pbs.twimg.com/card_img/862391238095327232/Xw8904Nz?format=jpg&name=600x314",
        link: "https://twitter.com/fullstory/status/862047561787551744"
      }
    ]

  }

    likeMe(i) {
    if (this.spaceScreens[i].liked == 0)
      this.spaceScreens[i].liked = 1;
    else
      this.spaceScreens[i].liked = 0;
  }

  deleteMe(i) {
    this.spaceScreens.splice(i,1);
    console.log(i);
  }
}

interface mainNav {
  name: string;
  link: string;
  active: boolean;
}
interface document {
  link: string;
  author: string;
  title: string;
  date: String;
  text: string;
  attachments: string;

}
